/*
 * @Descripttion : 
 * @Autor        : Lilong
 * @Date         : 2022-09-08 00:22:26
 * @LastEditTime : 2022-09-08 00:39:05
 * @FilePath     : \src\hooks\mouse.js
 */
import { ref, onMounted, onUnmounted } from 'vue'
import useEventListener from './event'

/**
 * 组合式函数作用：
 * 封装重复代码，实现复用。将逻辑移到外部，并返回需要暴露的状态
 * 
 * 注意：
 * 1、组合式函数中可以嵌套多个组合式函数
 * 2、可以在组合式函数中使用所有的组合式 API
 */


// 按照惯例，组合式函数名以“use”开头
const useMouse = () => {
  // 被组合式函数封装和管理的状态
  const x = ref(0)
  const y = ref(0)

  useEventListener(window, 'mousemove', (event) => {
    x.value = event.pageX
    y.value = event.pageY
  })

  // 通过返回值暴露所管理的状态,每一个调用 useMouse() 的组件实例会创建其独有的 x、y 状态拷贝，因此他们不会互相影响,如果要共享状态，可以用vuex
  return { x, y }
}

export default useMouse