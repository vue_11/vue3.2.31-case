/*
 * @Descripttion : 
 * @Autor        : Lilong
 * @Date         : 2022-09-08 00:36:06
 * @LastEditTime : 2022-09-08 00:37:40
 * @FilePath     : \src\hooks\event.js
 */
// event.js
import { onMounted, onUnmounted } from 'vue'

const useEventListener = (target, event, callback) => {
  // 如果你想的话，
  // 也可以用字符串形式的 CSS 选择器来寻找目标 DOM 元素
  // 一个组合式函数也可以挂靠在所属组件的生命周期上
  // 来启动和卸载副作用
  onMounted(() => target.addEventListener(event, callback))
  onUnmounted(() => target.removeEventListener(event, callback))
}

export default useEventListener
