/*
 * @Descripttion : 
 * @Autor        : Lilong
 * @Date         : 2022-09-09 00:43:54
 * @LastEditTime : 2022-09-09 00:43:57
 * @FilePath     : \响应性语法糖\组合式API中\vue.config.js
 */
const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  chainWebpack: (config) => {
    config.module
      .rule('vue')
      .use('vue-loader')
      .tap((options) => {
        return {
          ...options,
          reactivityTransform: true
        }
      })
  }
})