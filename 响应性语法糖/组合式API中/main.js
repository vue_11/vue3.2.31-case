/*
 * @Descripttion : 
 * @Autor        : Lilong
 * @Date         : 2022-09-09 00:44:07
 * @LastEditTime : 2022-09-09 00:44:09
 * @FilePath     : \响应性语法糖\组合式API中\main.js
 */
import { createApp } from 'vue'
import App from './App.vue'

const app = createApp(App)

app.mount('#app')
