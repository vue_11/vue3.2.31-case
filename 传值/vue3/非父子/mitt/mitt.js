/*
 * @Descripttion : 
 * @Autor        : Lilong
 * @Date         : 2022-09-14 10:13:19
 * @LastEditTime : 2022-09-14 10:17:34
 * @FilePath     : \src\mitt.js
 */
import mitt from 'mitt'
const Mitt = mitt()
export default Mitt