/*
 * @Descripttion : 
 * @Autor        : Lilong
 * @Date         : 2022-09-09 01:33:18
 * @LastEditTime : 2022-09-09 01:37:41
 * @FilePath     : \src\main.js
 */
import { createApp } from 'vue'
import App from './App.vue'
import router from './router'

import { createPinia } from "pinia";
const pinia = createPinia();

createApp(App).use(router).use(pinia).mount('#app')
