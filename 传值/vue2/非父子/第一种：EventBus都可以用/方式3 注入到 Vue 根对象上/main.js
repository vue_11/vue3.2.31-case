/*
 * @Descripttion : 
 * @Autor        : Lilong
 * @Date         : 2022-09-14 10:59:33
 * @LastEditTime : 2022-09-14 12:41:32
 * @FilePath     : \src\main.js
 */
import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  data: {
    Bus: new Vue()
  }
}).$mount('#app')
