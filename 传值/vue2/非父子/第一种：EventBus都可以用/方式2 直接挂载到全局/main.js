/*
 * @Descripttion : 
 * @Autor        : Lilong
 * @Date         : 2022-09-14 10:59:33
 * @LastEditTime : 2022-09-14 12:40:18
 * @FilePath     : \src\main.js
 */
import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false
Vue.prototype.$bus = new Vue()

new Vue({
  render: h => h(App),
}).$mount('#app')
