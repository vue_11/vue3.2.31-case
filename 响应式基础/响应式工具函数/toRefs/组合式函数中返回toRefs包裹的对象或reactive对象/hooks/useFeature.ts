/*
 * @Descripttion : 
 * @Autor        : Lilong
 * @Date         : 2022-10-09 16:54:02
 * @LastEditTime : 2022-10-09 16:54:59
 * @FilePath     : \src\hooks\useFeature.ts
 */
import {toRefs, reactive} from 'vue'

const useFeatureX = () => {
    const state = reactive({
      foo: 1,
      bar: 2
    })
    // 返回时转换为ref
    return toRefs(state)
}

export default useFeatureX