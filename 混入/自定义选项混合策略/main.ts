// src/main.ts

import App from './App.vue'
import { createApp } from 'vue'
import { myMixin } from './mixins/global/myMixin.g'

const app = createApp(App)


app.config.optionMergeStrategies.custom = (toVal, fromVal) => {
  //fromVal为组件中的，toVal为mixin中的
  //console.log(toVal, fromVal)
  // return fromVal || toVal //取组件中的custom，默认也是此规则
  return toVal || fromVal  //取mixin中的custom
}

app.mixin(myMixin)

app.mount('#app')