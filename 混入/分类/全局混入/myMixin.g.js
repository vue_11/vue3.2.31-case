// src/mixins/global/myMixin.g.js

const myMixin = {
    created() {
        const myOption = this.$options.myOption
        if (myOption) {
          console.log(myOption)
        }
    }
}

export {
  myMixin
}