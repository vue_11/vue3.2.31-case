import App from './App.vue'
import { createApp } from 'vue'
import { myMixin } from './mixins/global/myMixin.g'

const app = createApp(App)

app.mixin(myMixin)  //全局混入

app.mount('#app')

