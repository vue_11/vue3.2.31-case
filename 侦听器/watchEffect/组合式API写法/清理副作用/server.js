// 当前目录 node server.js启动

const http = require('http');

const server = http.createServer((req, res) => {
  res.setHeader('Access-Control-Allow-Origin', "*");
  res.setHeader('Access-Control-Allow-Credentials', true);
  res.setHeader('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
  res.writeHead(200, { 'Content-Type': 'application/json'});
});

server.listen(3001, () => {
  console.log('Server is running...');
});

server.on('request', (req, res) => {
  setTimeout(() => {
    if (/\d.json/.test(req.url)) {
      const data = {
        content: '我是返回的内容，来自' + req.url
      }
      res.end(JSON.stringify(data));
    }
  }, Math.random() * 3000);
});