//main.ts中
import Counter from './Counter.vue'
import { createApp } from 'vue'

const App = createApp(Counter)

App.mount('#app')