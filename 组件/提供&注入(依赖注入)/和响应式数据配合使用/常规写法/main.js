/*
 * @Descripttion : 
 * @Autor        : Lilong
 * @Date         : 2022-09-07 16:13:15
 * @LastEditTime : 2022-09-07 16:13:17
 * @FilePath     : \组件\提供&注入(依赖注入)\注入别名\main.js
 */
import { createApp } from 'vue'
import App from './App.vue'

createApp(App).mount('#app')
