// src/main.ts

import App from './App.vue'
import { createApp } from 'vue'
import TodoItem  from './components/todo-item.vue'

const app = createApp(App)

app.component('TodoItem', TodoItem)

app.mount('#app')