// src/main.ts

import App from './App.vue'
import { createApp } from 'vue'
import * as Directives from './directives/index.js'

const app = createApp(App)

Object.keys(Directives).forEach((item) => {
  const obj = Directives[item]
  app.directive(item, obj)
})


app.mount('#app')