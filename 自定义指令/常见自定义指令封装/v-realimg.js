const realimg = {
    async beforeMount(el, binding) {
        const imgURL = binding.value;
        if (imgURL) {
          const exist = await imageIsExist(imgURL);
          exist && el.setAttribute('src', imgURL);
        }
        // 判断一个图片是否存在, 注意是异步行为
        function imageIsExist(url) {
            return new Promise(resolve => {
                let img = new Image();
                img.src = url;
                img.onload = () => {
                    if(img.complete) {
                        resolve(true);
                        img = null;
                    }
                }
                img.onerror = () => {
                    resolve(false);
                    img = null;
                }
            })
        }
    }
}

export default realimg

/**
 * 原方案：
 * 采用<img/>标签的onerror事件，在图片请求失败的时候显示另外一张图片来替代，那么当另外一张图片也请求失败或者质量很大时，也会触发onerror事件
 * 导致陷入触发这个事件的死循环中，最后造成页面卡死。就算再显示完失败的图片后，将onerror置为null来防止进入死循环，但仍然没有指令方便
 * 
 * 思路：
 * 1、一种是直接加载目标图片，等到加载失败的时候使用默认图片
 * 2、一种是直接加载默认图片，等图片加载完成之后再使用加载完成的目标图片
 * 
 * 此处选择第二种，直接加载默认图片起到占位效果，如果想反之，只需对换两种途径即可
 */