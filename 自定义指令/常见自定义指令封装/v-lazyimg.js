const lazyimg = {
    beforeMount(el, binding) {
        el.$data_src = binding.value;
    },
    mounted(el) {
        IntersectionObserver ? ioEvent(el) : scrollEvent(el);
    },
    updated(el, binding) {
        el.$data_src = binding.value;
    },
    unmounted(el) {
        // 实时更新最新的图片路径
        IntersectionObserver && el.$io.disconnect();
    }
}

function ioEvent(el) {
    //IntersectionObserver不兼容IE
    const io = new IntersectionObserver(entries => {
      const realSrc = el.$data_src;
      entries[0].isIntersecting && realSrc && (el.src = realSrc);
    });
    el.$io = io;
    io.observe(el);
}
  
function scrollEvent(el) {
    const handler = throttler(loadImg, 250);
    loadImg(el);
    window.addEventListener('scroll', () => {
        handler(el);
    })
}

// 滚动触发后, 需要处理的后续逻辑
function loadImg(el) {
    const clientHeight = getClientHeight();
    // 获取到元素距离屏幕顶部的距离(top) 与 元素距离屏幕底部的距离(bottom)。
    const {top, bottom} = el.getBoundingClientRect();
    const realSrc = el.$data_src;
    (top < clientHeight && bottom > 0) && realSrc && (el.src = realSrc);
}

//获取视口高度, 兼容不同浏览器厂商
function getClientHeight(){
    const dClientHeight = document.documentElement.clientHeight;
    const bodyClientHeight = document.body.clientHeight;
    let clientHeight = 0;
    if (bodyClientHeight && dClientHeight) {
        clientHeight = bodyClientHeight < dClientHeight ? bodyClientHeight : dClientHeight;
    } else {
        clientHeight = bodyClientHeight > dClientHeight ? bodyClientHeight : dClientHeight;
    }
    return clientHeight;
}

//节流函数
function throttler(fun, delay) {
    let last, deferTimer
    return function (args) {
        let that = this
        let _args = arguments
        let now = +new Date()
        if (last && now < last + delay) {
            clearTimeout(deferTimer)
            deferTimer = setTimeout(function () {
                last = now
                fun.apply(that, _args)
            }, delay)
        }else {
            last = now
            fun.apply(that,_args)
        }
    }
}

export default lazyimg