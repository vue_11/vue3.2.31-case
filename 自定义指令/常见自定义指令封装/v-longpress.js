const longpress = {
    beforeMount(el, binding) {
        const cb = binding.value;
        el.$duration = binding.arg || 3000; // 获取长按时长, 默认3秒执行长按事件
        if(typeof cb !== 'function') return console.warn('v-longpress指令必须接收一个回调函数');
        let timer = null;
        const add = (e) => {
          // 排除点击与右键情况, event.button: 0-左键  2-右键
          if(e.type === 'click' && e.button !== 0) return;
          e.preventDefault();
          if(timer === null) {
            timer = setTimeout(() => {
              cb();
              timer = null;
            }, el.$duration)
          }
        }
        const cancel = () => {
          if (timer !== null) {
            clearTimeout(timer);
            timer = null;
          }
        }
    
        // 添加计时器
        el.addEventListener('mousedown', add);
        el.addEventListener('touchstart', add);
        // 取消计时器
        el.addEventListener('click', cancel);
        el.addEventListener('mouseout', cancel);
        el.addEventListener('touchend', cancel)
        el.addEventListener('touchcancel', cancel)
    },

    updated(el, binding) {
        // 可以实时更新时长
        el.$duration = binding.arg;
    },
    unmounted(el) {
        el.removeEventListener('mousedown', () => {});
        el.removeEventListener('touchstart', () => {});
        el.removeEventListener('click', () => {});
        el.removeEventListener('mouseout', () => {});
        el.removeEventListener('touchend', () => {});
        el.removeEventListener('touchcancel', () => {});
    }
}

export default longpress


/**
 * 思路：
 * 1、当用户按下鼠标左键触发 mousedown() 事件，则我们启动一个计时器，设定一个时间阈值，开始计时。
 * 2、如果在阈值内 mouseup() 事件被触发了，我们就认为只是个普通点击事件，如果超过阈值后才触发事件则我们认为是长按事件
 * 
 * 移动端：可能有场景是长按3秒或者5秒再做某件事情
 */