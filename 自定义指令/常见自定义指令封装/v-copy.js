const copy = {
    beforeMount(el, binding) {
        el.targetContent = binding.value;
        const success = binding.arg;
        el.addEventListener('click', () => {
            if(!el.targetContent) return console.warn('没有需要复制的目标内容');
            // 创建textarea标签
            const textarea = document.createElement('textarea');
            // 设置相关属性
            textarea.readOnly = 'readonly';
            textarea.style.position = 'fixed';
            textarea.style.top = '-99999px';
            // 把目标内容赋值给它的value属性
            textarea.value = el.targetContent;
            // 插入到页面
            document.body.appendChild(textarea);
            // 调用onselect()方法
            textarea.select();
            // 把目标内容复制进剪贴板, 该API会返回一个Boolean
            const res = document.execCommand('Copy');
            res && success ? success(el.targetContent) :  console.log('复制成功，剪贴板内容：' + el.targetContent);
            // 移除textarea标签
            document.body.removeChild(textarea);
        })
    }, 
    updated(el, binding) {
        // 实时更新最新的目标内容
        el.targetContent = binding.value;
    },
    unmounted(el) {  
        el.removeEventListener('click', ()=>{})
    }
}

export default copy

/**
 * 思路：
 * 1、首先，使用场景可能是我们点击某一个按钮，就复制了某个内容（目标内容）到剪贴板中了，通过 ctrl+v 能粘贴出来。
 * 2、把内容塞进剪贴板，我们会用到上面提到的document.execCommand('Copy')API来实现，但是这里要注意，该API的作用是将当前 选中 的内容拷贝进剪贴板，所以我们必须让我们的目标内容被选中，才能调用该API来完成功能。
 * 3、让内容被选中我们能通过 HTML事件 中的 onselect() 方法来实现，而 <input /> 标签、<textarea /> 标签都能支持该事件。
 * 4、所以我们需要动态创建一个 <textarea /> 标签，当然该标签只是个辅助工具，所以要把它移出可视区域外
 * 5、再将我们的目标内容赋值给它的value属性，将它插入到页面DOM结构中。
 * 6、调用 <textarea /> 标签的 onselect() 选中值，再通过 document.execCommand('Copy') API把内容复制进剪贴板。
 * 7、最后移除 <textarea /> 标签就可以。
 */