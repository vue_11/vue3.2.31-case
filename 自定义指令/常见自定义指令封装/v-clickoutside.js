const clickoutside = {
    beforeMount(el, binding) {
        document.addEventListener('click', (e) => {
          el.contains(e.target) && binding.value();
        }, false)
    },
    unmounted() {
        document.removeEventListener('click', () => {})
    }
}

export default clickoutside

/**
 * 实现点击其它地方消失
 */