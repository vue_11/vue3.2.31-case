// src/directives/index.js

import copy from './v-copy'
import realimg from './v-realimg'
import lazyimg from './v-lazyimg'
import emoji from './v-emoji'
import longpress from './v-longpress'
import input from './v-input'
import draggable from './v-draggable'
import permission from './v-permission'
import loading from './v-loading'
import clickoutside from './v-clickoutside'

export {
    copy,
    realimg,
    lazyimg,
    emoji,
    longpress,
    input,
    draggable,
    permission,
    loading,
    clickoutside
}