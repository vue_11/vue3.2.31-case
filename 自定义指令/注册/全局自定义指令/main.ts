import App from './App.vue'
import { createApp } from 'vue'

const app = createApp(App)

//全局自定义指令
app.directive('focus', {
  // 当被绑定的元素插入到 DOM 中时……
  mounted(el) {
    // Focus the element
    el.focus()
  }
})

app.mount('#app')