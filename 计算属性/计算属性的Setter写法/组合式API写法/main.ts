// src/main.ts

import App from './App.vue'
import { createApp } from 'vue'

const app = createApp(App)

const vm = app.mount('#app')

vm.fullName = 'John Doe'
vm.name.firstName = 'Allen'